import React from './node_modules/react';
import '../SASS/Homepage.scss';

function Homepage(){
    return(
        <div className="pages">
            {
                //Ini Navbar
            }
            <div className="navbar">
                <div className="logo"></div>
                
                {
                    //Ini Navbar Item
                }
                <div className="navItem"></div>
            </div>
    
            {
                //Ini Konten
            }
            <div className="content">
                <div className="backgroundImages"></div>
                <div className="loginBox"></div>
            </div>
        </div>
    );
}

export default Homepage;