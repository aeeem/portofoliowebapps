import React, { useState } from 'react';
import {Link} from 'react-router-dom';
import {checkAuth} from '../API/apiLogin.js';


function handleChange(state,callback,event) {
  switch(state){
    case state="username" :
      callback(event.target.value,"inputUsername")
      break;
    case state="password" :
      callback(event.target.value, "inputPassword")
      break;
  }
}
function Navbar() {
  const [inputPassword,setInputPassword] = useState(" ");
  const [inputUsername,setInputUsername] = useState(" ");
  return (
    <div className="form small bold-shadow">
        <div className="form-items title">Welcome Back !</div>
        <div className="form-input">
          <div className="form-items">
            <div>Email / Username {inputUsername}</div>
            <input type="text" name="userName" onChange={(e)=>handleChange("username",setInputUsername,e)}/>
          </div>
          <div className="form-items">
            <div>Password</div>
            <input type="password" name="password" onChange={(e)=>handleChange("password",setInputPassword,e)}/>
          </div>
        </div>
        <div className="form-items button-group">
          <button onClick={() => checkAuth(inputUsername,inputPassword)}>Login</button>
          <Link><div>Register a new account!</div></Link>
        </div>
    </div>
  );
}

export default Navbar;