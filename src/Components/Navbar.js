import React from 'react';
import Logo from '../Assets/logo.svg'

function Navbar() {
  return (
    <div className="navbar">
        <img className="logo" src={Logo} />
        <img className="watermark" src={Logo} />
    </div>
  );
}

export default Navbar;