import React from 'react';
import AdminHome from './Pages/HomepageAdmin.js';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App() {
  return (
    <Router>
      <Route path="/" component={AdminHome}/>
    </Router>
  );
}

export default App;
