import React from '../../node_modules/react';
import Nav from '../Components/Navbar';
import '../SASS/main-css.scss';
import Arts from '../Assets/homepageadmin.svg';
import Form from '../Components/Form';

function HomepageAdmin() {
  return (
    <div class="pages">
        <Nav />
        <div className="content">
        <img src={Arts} />
        <Form />
        </div>
    </div>
  );
}

export default HomepageAdmin;